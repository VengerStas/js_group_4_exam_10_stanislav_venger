const express = require('express');
const config = require('../config');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `newList`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send(results);
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `newList` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }

            if (results[0]) {
                res.send(results[0])
            } else {
                res.status(404).send({error: 'News not found'});
            }
        })
    });

    router.post('/', upload.single('image'), (req, res) => {
        const newsItem = req.body;
        const datetime = new Date().toISOString();

        if (req.file) {
            newsItem.image = req.file.filename;
        }

        connection.query('INSERT INTO `newList` (`news_title`, `news_desc`, `image`, `news_date`) VALUES (?, ?, ?, ?)',
            [newsItem.news_title, newsItem.news_desc, newsItem.image, datetime],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'OK'});
            });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `newList` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send({message: 'Delete ok'});
        });

    });

    return router;
};

module.exports = createRouter;