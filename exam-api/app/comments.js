const express = require('express');

const createRouter = connection => {
    const router = express.Router();

    router.get('/:id', (req, res) => {
        connection.query('SELECT `idComment`, `author`,`comment` FROM `commentsList` INNER JOIN `newList` ON `commentsList`.`newsId` = `newList`.`id` WHERE `newList`.`id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send(results);
        })
    });

    router.post('/', (req, res) => {
        const commentItem = req.body;
        req.body.author = req.body.author || "Anonymous";

        connection.query('INSERT INTO `commentsList` (`newsId`, `author`, `comment`) VALUES (?, ?, ?)',
            [commentItem.newsId, commentItem.author, commentItem.comment],
            (error, results) => {
                if (error) {
                    console.log(error);
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'OK'});
            });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `commentsList` WHERE `idComment` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send({message: 'Delete ok'});
        });
    });

    return router;
};

module.exports = createRouter;