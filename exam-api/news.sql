CREATE SCHEMA `news` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `news`.`newList` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_title` VARCHAR(255) NOT NULL,
  `news_desc` TEXT NOT NULL,
  `image` VARCHAR(100) NULL,
  `news_date` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `news`.`commentsList` (
    `idComment` INT NOT NULL AUTO_INCREMENT,
    `newsId` INT NOT NULL,
    `author` VARCHAR(100) NULL,
    `comment` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`idComment`),
    INDEX `news_id_fk_idx` (`newsId` ASC),
    CONSTRAINT `news_id_fk`
      FOREIGN KEY (`newsId`)
      REFERENCES `news`.`newList` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE);