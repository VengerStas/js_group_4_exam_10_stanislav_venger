import {
    DELETE_NEWS,
    NEWS_FAILURE,
    NEWS_REQUEST,
    NEWS_REQUEST_SUCCESS, POST_REQUEST_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
    news: [],
    post: {}
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case DELETE_NEWS:
            return {
                ...state,
            };
        case NEWS_REQUEST:
            return {
                ...state,
            };
        case NEWS_REQUEST_SUCCESS:
            return {
                ...state,
                news: action.news
            };
        case POST_REQUEST_SUCCESS:
            return {
                ...state,
                post: action.post
            };
        case NEWS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};

export default newsReducer;