import {
    COMMENT_FAILURE,
    COMMENT_REQUEST,
    COMMENT_REQUEST_SUCCESS,
    DELETE_COMMENTS,
} from "../actions/actionTypes";

const initialState = {
    comments: [],
};

const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case COMMENT_REQUEST:
            return {
                ...state,
            };
        case COMMENT_REQUEST_SUCCESS:
            return {
                ...state,
                comments: action.comments,
            };
        case COMMENT_FAILURE:
            return {
                ...state,
            };
        case DELETE_COMMENTS:
            return {
                ...state,
            };
        default:
            return state;
    }
};

export default commentReducer;