import axios from '../../axios-news';
import {COMMENT_FAILURE, COMMENT_REQUEST, COMMENT_REQUEST_SUCCESS} from "./actionTypes";
import {fetchGetNews, newsFailure, newsRequest} from "./newsBuilder";


export const commentRequest = () => {
    return {type: COMMENT_REQUEST};
};

export const commentSuccess = comments => {
    return {type: COMMENT_REQUEST_SUCCESS, comments};
};

export const commentFailure = error => {
    return {type: COMMENT_FAILURE, error};
};

export const fetchGetComment = (idNewsItem) => {
    return (dispatch, getState) => {
        dispatch(commentRequest());
        axios.get('/comments/' + idNewsItem).then(response => {
            dispatch(commentSuccess(response.data));
        }, error => {
            dispatch(commentFailure(error));
        })
    }
};

export const fetchCommentPost = data => {
    return (dispatch) => {
        axios.post('/comments', data).then(() => {
            dispatch(newsRequest());
        }, error => {
            dispatch(newsFailure(error));
        })
    }
};

export const deleteComment = (idComment) => {
    return (dispatch) => {
        dispatch(newsRequest());
        axios.delete('/comments/' + idComment).then(() => {
            dispatch(fetchGetNews());
        })
    }
};