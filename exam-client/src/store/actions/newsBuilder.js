import axios from '../../axios-news';
import {NEWS_FAILURE, NEWS_REQUEST, NEWS_REQUEST_SUCCESS, POST_REQUEST_SUCCESS} from "./actionTypes";

export const newsRequest = () => {
    return {type: NEWS_REQUEST};
};

export const newsSuccess = news => {
    return {type: NEWS_REQUEST_SUCCESS, news};
};

export const postSuccess = post => {
    return {type: POST_REQUEST_SUCCESS, post}
};


export const newsFailure = error => {
    return {type: NEWS_FAILURE, error};
};

export const fetchGetNews = () => {
    return (dispatch, getState) => {
        dispatch(newsRequest());
        axios.get('/news').then(response => {
            dispatch(newsSuccess(response.data));
        }, error => {
            dispatch(newsFailure(error));
        })
    }
};

export const fetchGetPost = itemId => {
    return (dispatch) => {
        axios.get('/news/' + itemId).then((response) => {
            dispatch(postSuccess(response.data));
        })
    }
};

export const fetchNewsPost = data => {
    return (dispatch) => {
        axios.post('/news', data).then(() => {
            dispatch(newsRequest());
            dispatch(fetchGetNews());
        }, error => {
            dispatch(newsFailure(error));
        })
    }
};

export const deleteItemNews = (idNews) => {
    return (dispatch) => {
        dispatch(newsRequest());
        axios.delete('/news/' + idNews).then(() => {
            dispatch(fetchGetNews());
        })
    }
};