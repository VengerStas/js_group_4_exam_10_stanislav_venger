import React, {Component, Fragment} from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";
import {Button, Card, CardBody, CardText, CardTitle, Container, NavLink} from "reactstrap";
import {deleteItemNews, fetchGetNews} from "../../store/actions/newsBuilder";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";

import './News.css';

class News extends Component {
    componentDidMount() {
        this.props.loadNews();
    };

    deleteHandler = (idNews) => {
        this.props.deleteNewsItem(idNews);
        this.props.history.push('/');
    };

    render() {
        console.log(this.props.newsList);
        let newsItem = null;
        if (this.props.newsList) {
            newsItem = this.props.newsList.map(item => {
                return (
                    <Card className="news-item" key={item.id}>
                        <NewsThumbnail image={item.image}/>
                        <CardBody>
                            <CardTitle className="news-title">{item.news_title}</CardTitle>
                            <CardText>{item.news_date}</CardText>
                            <Button><NavLink href={'/' + item.id}>Read Full Post >></NavLink></Button>
                            <Button className="news-delete" onClick={() => this.deleteHandler(item.id)}>Delete</Button>
                        </CardBody>
                    </Card>
                )
            })
        }
        return (
            <Fragment>
                <Container>
                    <div className="news-list-block">
                        <div className="news-block-header">
                            <h4 className="news-block-title">News</h4>
                            <div className="header-controls">
                                <h2 className="posts">Posts</h2>
                                <Button className="add-news" tag={RouterNavLink} to="/add-news">Add new post</Button>
                            </div>
                        </div>
                        <div className="news-list">
                            {newsItem}
                        </div>
                    </div>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    newsList: state.news.news,
});

const mapDispatchToProps = dispatch => ({
    loadNews: () => dispatch(fetchGetNews()),
    deleteNewsItem: (idNews) => dispatch(deleteItemNews(idNews)),
});

export default connect(mapStateToProps, mapDispatchToProps)(News);
