import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Container} from "reactstrap";
import {fetchGetPost} from "../../store/actions/newsBuilder";
import CommentsForm from "../../components/CommentsForm/CommentsForm";
import {deleteComment, fetchGetComment} from "../../store/actions/commentBuilder";

import './NewsInfo.css';

class NewsInfo extends Component {
    componentDidMount() {
        this.props.loadPost(this.props.match.params.id);
        this.props.loadComment(this.props.match.params.id);
    };

    deleteHandler = (idComment) => {
        this.props.deleteComment(idComment);
        this.props.history.push('/');
    };

    render() {
        let comment = this.props.comment.map(comment => {
            return (
                <div className="comment-item" key={comment.idComment}>
                    <span><strong>{comment.author}</strong> wrote:</span>
                    <span>{comment.comment}</span>
                    <Button type="submit" onClick={() => this.deleteHandler(comment.idComment)}>Delete</Button>
                </div>
            )
        });
        return (
            <div className="news-item-post">
                <Container>
                   <div>
                       <h2>{this.props.post.news_title}</h2>
                       <p>{this.props.post.news_date}</p>
                       <p>{this.props.post.news_desc}</p>
                   </div>
                    <div  className="comments-block">
                        <h4>Comments</h4>
                        {
                            comment
                        }
                    </div>
                    <CommentsForm
                        newsId={this.props.match.params.id}
                    />
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    post: state.news.post,
    comment: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
    loadPost: (itemId) => dispatch(fetchGetPost(itemId)),
    loadComment: (idNewsItem) => dispatch(fetchGetComment(idNewsItem)),
    deleteComment: (idComment) => dispatch(deleteComment(idComment)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsInfo);