import React, {Component} from 'react';
import NewsForm from "../../components/NewsForm/NewsForm";

class AddNews extends Component {
    render() {
        return (
            <div>
                <NewsForm/>
            </div>
        );
    }
}

export default AddNews;