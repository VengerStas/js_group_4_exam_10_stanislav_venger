import React, {Component} from 'react';
import {Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {fetchNewsPost} from "../../store/actions/newsBuilder";

class NewsForm extends Component {
    state = {
        news_title: '',
        news_desc: '',
        image: null,
        news_date: '',
    };

    submitNewsHandler = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });
        this.props.addNews(formData);
    };

    changeValue = event => {
        this.setState ({
            [event.target.name]: event.target.value
        });
    };

    changeImage = event => {
        this.setState ({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <div className="add-news">
                <Container>
                    <h4>Add news</h4>
                    <Form>
                        <FormGroup>
                            <Label for="title">Title</Label>
                            <Input type="text" name="news_title" id="title"
                                   value={this.state.news_title} onChange={this.changeValue} placeholder="Enter a title for news" required/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="desc">Description</Label>
                            <Input type="text" name="news_desc" id="desc"
                                   alue={this.state.news_desc} onChange={this.changeValue} placeholder="Enter a description for news" required/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="image">File</Label>
                            <Input type="file" name="image" id="image" onChange={this.changeImage}/>
                        </FormGroup>
                        <Button type="submit" onClick={(event) => this.submitNewsHandler(event)}>Send</Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news.news
});

const mapDispatchToProps = dispatch => ({
    addNews: data => dispatch(fetchNewsPost(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsForm);