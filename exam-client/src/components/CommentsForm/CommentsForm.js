import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {fetchCommentPost} from "../../store/actions/commentBuilder";

import './CommentsForm.css';

class CommentsForm extends Component {
    state = {
        newsId: this.props.newsId,
        author: '',
        comment: ''
    };


    submitCommentHandler = event => {
        event.preventDefault();
        const data = {
            newsId: this.props.newsId,
            author: this.state.author,
            comment: this.state.comment
        };
        this.props.addComment(data);
    };

    changeValue = event => {
        this.setState ({
            [event.target.name]: event.target.value
        });
    };


    render() {
        return (
            <div className="comment-form">
                <h4>Add comment</h4>
                <Form>
                    <FormGroup>
                        <Label for="title">Name</Label>
                        <Input type="text" name="author" id="author"
                               value={this.state.author} onChange={this.changeValue} placeholder="Enter your name" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="desc">Comment</Label>
                        <Input type="text" name="comment" id="comment"
                               value={this.state.comment} onChange={this.changeValue} placeholder="Enter your comment" required/>
                    </FormGroup>
                    <Button type="submit" onClick={(event) => this.submitCommentHandler(event)}>Send</Button>
                </Form>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    comments: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
    addComment: data => dispatch(fetchCommentPost(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentsForm);