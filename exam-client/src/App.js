import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";
import News from "./containers/News/News";
import AddNews from "./containers/AddNews/AddNews";
import NewsInfo from "./containers/NewsInfo/NewsInfo";

import './App.css';


class App extends Component {
  render() {
    return (
        <Switch>
          <Route path='/' exact component={News} />
          <Route path='/add-news' exact component={AddNews}/>
          <Route path='/:id' exact component={NewsInfo}/>
          <Route render={() => <h1>Page not found</h1>}/>
        </Switch>
    );
  }
}

export default App;
